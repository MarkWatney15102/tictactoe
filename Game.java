package tictactoe;

import javax.swing.JButton;

public class Game {
    private Spieler[] spieler;
    private Spielregeln spielregeln;
    
    public Game() {
        this.spielregeln = new Spielregeln();
        this.spieler = new Spieler[2];
        this.spieler[0] = new Spieler('X');
        this.spieler[1] = new Spieler('O');
        
        this.spieler[0].setTurn(true);
        this.spieler[1].setTurn(false);
    }
    
    public void setPlayerName(int playerId, String name) throws Exception {
        switch (playerId) {
            case 1:
                this.spieler[0].setName(name);
                break;
            case 2:
                this.spieler[1].setName(name);
                break;
            default:
                throw new Exception("Player dont exists");
        }
    }
    
    public void setPlayerChar(int x, int y, JButton button) {
        char playerChar = '-';
        if (this.spieler[0].isTurn()) {
            this.spielregeln.checkHasPlayerWon(this.spieler[0]);
            this.spieler[0].setTurn(false);
            this.spieler[1].setTurn(true);
            playerChar = this.spieler[0].getPlayerChar();
        } else {
            this.spielregeln.checkHasPlayerWon(this.spieler[1]);
            this.spieler[1].setTurn(false);
            this.spieler[0].setTurn(true);
            playerChar = this.spieler[1].getPlayerChar();
        }
        
        this.spielregeln.setPlayerChar(playerChar, x, y);
        button.setText("" + playerChar);
    }
    
    public Spieler[] getSpieler() {
        return this.spieler;
    }
}
