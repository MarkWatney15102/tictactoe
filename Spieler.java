package tictactoe;

public class Spieler {
    private String name;
    private boolean turn;
    private char playerChar;
    
    public Spieler(char playerChar) {
        this.playerChar = playerChar;
    }

    public boolean isTurn() {
        return this.turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public char getPlayerChar() {
        return this.playerChar;
    }
}
